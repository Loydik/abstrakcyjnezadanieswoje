﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using Queue;

namespace PrioritizedQueueTests
{
    public class Tests
    {
        [Test]
        public void CheckThatPrioritizedQueueImplementsIEnumerable()
        {
            var maybeIenumerable = typeof (PrioritizedQueue<string>).GetInterface("IEnumerable`1");

            Assert.That(maybeIenumerable, Is.Not.Null, "Type does not implement IEnumerable");
        }

        [Test]
        public void CheckThatElementsArePoppedCorrectlyWithDefaultComparer()
        {
            PrioritizedQueue<string> queue = new PrioritizedQueue<string>();
            queue.Enqueue("foo");
            queue.Enqueue("foolonger");
            queue.Enqueue("");
            queue.Enqueue("somecooltext");

            List<string> expectedPopOrder = new List<string> {"somecooltext", "foolonger", "foo", ""};

            for (int i = 0; i < 3; i++)
            {
                Assert.That(queue.DequeueLargest(), Is.EqualTo(expectedPopOrder.ElementAt(i)),
                    "The order of dequeing is incorrect");
            }
        }

        [Test]
        public void CheckThatElementsArePoppedCorrectlyWithCustomComparer()
        {
            PrioritizedQueue<string> queue = new PrioritizedQueue<string>(new StringLengthComparer<string>());
            queue.Enqueue("foo");
            queue.Enqueue("foolonger");
            queue.Enqueue("");
            queue.Enqueue("somecooltext");

            List<string> expectedPopOrder = new List<string> {"", "foo", "foolonger", "somecooltext" };

            for (int i = 0; i < 3; i++)
            {
                Assert.That(queue.DequeueSmallest(), Is.EqualTo(expectedPopOrder.ElementAt(i)),
                    "The order of dequeing is incorrect");
            }
        }

        [Test]
        public void CheckBinarySearch()
        {
            PrioritizedQueue<int> queue = new PrioritizedQueue<int>();

            queue.Enqueue(4);
            queue.Enqueue(3);
            queue.Enqueue(2);
            queue.Enqueue(1);

            var index = queue.BinarySearch(2);

            Assert.That(index, Is.EqualTo(1), "Binary search is working improperly");
        }

        [Test]
        public void CheckClearMethod()
        {
            PrioritizedQueue<string> queue = new PrioritizedQueue<string>();

            queue.Enqueue("ala");
            queue.Enqueue("ma");
            queue.Enqueue("kota");

            queue.Clear();

            Assert.That(queue.Count, Is.EqualTo(0));
        }

        [Test]
        public void CheckToArrayMethod()
        {
            PrioritizedQueue<string> queue = new PrioritizedQueue<string>();

            queue.Enqueue("ala");
            queue.Enqueue("ma");
            queue.Enqueue("kota");


            Assert.That(queue.ToArray(), Is.EqualTo(new[] {"ala", "kota","ma"}));
        }
    }

    

    #region Nested StringLengthComparer class

    class StringLengthComparer<T> : IComparer<T>
    {
        public int Compare(T x, T y)
        {
            if (typeof (T) == typeof (string))
            {
                var string1 = x as string;
                var string2 = y as string;

                if (string1.Length == string2.Length)
                {
                    return 0;
                }

                if (string1.Length > string2.Length)
                {
                    return 1;
                }

                return -1;
            }

            else
            {
                throw new ArgumentException();
            }
        }
    }

    #endregion

}
