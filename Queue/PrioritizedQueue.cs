﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Queue
{
    /// <summary>
    /// Custom Priority Queue implementation. It works similar to a regular Queue but returns objects
    /// based on a specific order
    /// </summary>
    public class PrioritizedQueue<T> : IEnumerable<T>
    {

        #region Private Members

        private readonly List<T> _elements = new List<T>();
        private readonly IComparer<T> _comparer;

        #endregion


        #region Construction

        /// <summary>
        /// Default Constructor
        /// </summary>
        public PrioritizedQueue()
        {
        }

        /// <summary>
        /// Constructor accepting IComparer
        /// </summary>
        public PrioritizedQueue(IComparer<T> comparer)
        {
            _comparer = comparer;
        }

        #endregion



        #region Properties
        
        /// <summary>
        /// Protected Elements property
        /// </summary>
        protected List<T> Elements => _elements;

        /// <summary>
        /// PrioritizedQueue items count
        /// </summary>
        public int Count => _elements.Count;

        #endregion


        #region Public Methods

        /// <summary>
        /// Removes all elements from PrioritizedQueue
        /// </summary>
        public void Clear()
        {
            _elements.Clear();
        }

        /// <summary>
        /// Clones PrioritizedQueue object
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            // Make a new PQ and give it the same comparer.
            PrioritizedQueue<T> newPQueue = new PrioritizedQueue<T>(_comparer);
            newPQueue.CopyTo(_elements.ToArray(), 0);
            return newPQueue;
        }

        /// <summary>
        /// Searches for the specified object and returns the zero-based index of the first occurrence
        /// </summary>
        public int IndexOf(T item)
        {
            return (_elements.IndexOf(item));
        }

        /// <summary>
        /// Checks whether PrioritizedQueue contains a specific item
        /// </summary>
        /// <returns></returns>
        public bool Contains(T item)
        {
            return (_elements.Contains(item));
        }

        /// <summary>
        /// Searches for a element using a comparer 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int BinarySearch(T item)
        {
            return (_elements.BinarySearch(item, _comparer));
        }

        /// <summary>
        /// Checks whether Queue contains an element (using comparer)
        /// </summary>
        public bool Contains(T item, IComparer<T> comparer)
        {
            return (_elements.BinarySearch(item, comparer) >= 0);
        }

        /// <summary>
        /// Copies a Queue to Array
        /// </summary>
        public void CopyTo(T[] array, int index)
        {
            _elements.CopyTo(array, index);
        }

        /// <summary>
        /// Returns an array
        /// </summary>
        /// <returns></returns>
        public T[] ToArray()
        {
            return _elements.ToArray();
        }


        #region Enqueing, Dequeing and Peeking

        public void Enqueue(T item)
        {
            _elements.Add(item);
            _elements.Sort(_comparer);
        }

        public T DequeueLargest()
        {
            var index = _elements.Count - 1;
            T item = _elements.ElementAt(index);
            _elements.RemoveAt(index);
            return item;
        }

        public T PeekLargest()
        {
            return _elements[_elements.Count - 1];
        }

        public T DequeueSmallest()
        {
            var index = 0;
            T item = _elements.ElementAt(index);
            _elements.RemoveAt(index);
            return item;
        }

        public T PeekSmallest()
        {
            return _elements[0];
        }

        #endregion


        #endregion


        #region IEnumerator Members 

        public IEnumerator GetEnumerator()
        {
            return (_elements.GetEnumerator());
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return (_elements.GetEnumerator());
        }

        #endregion

    }
}
